const handler = require('./controllers/index');
const excelToJson = require('convert-excel-to-json');

(async() => {
    console.log('GetPaymentStatus is started!')
    const mockData = excelToJson({
        sourceFile: './data.xlsx'
    })
    
    mockData.details.forEach(async(item) => {
        const resp = await handler.checkPaymentStatus(item.A)
        // console.log(resp['data']['result']['Item'])
        if(resp) {
            console.log(`token ${item.A}, ${resp['data']['result']['Item']['lastUpdatedDate']}`)
        }
    })
})()