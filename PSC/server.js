const handler = require('./controllers/index');
const excelToJson = require('convert-excel-to-json');

(async() => {
    const mockData = excelToJson({
        sourceFile: './data.xlsx'
    })

    mockData.details.forEach(async (item) => {
        const resp = await handler.paymentSessionCallback(item)
        console.log(`token ${item.A}, ${resp['data']['result']['notificationResponse']}`)
    })
})()