const axios = require('axios');

const paymentSessionCallback = async (details) => {
    const data = {
        notification: {
            name: "PaymentProcessed",
            payload: {
                paymentId: details.A,
                accounts: [
                    {
                        status: details.B
                    }
                ]
            }
        },
        note: "RITM0656065"
    }

    const domain = 'https://cxs.globe.com.ph';
        try {
            const res = await axios.post(`${domain}/v1/paymentManagement/payments/sessions/callback`, data, {
                headers: {
                    "Content-Type": "application/json",
                    "User-Agent": "PostmanRuntime/7.35.0",
                    "Accept": "*/*",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Connection": "keep-alive",
                    "x-api-key": "6s04pimg2nq7sepocpa4ubajhb"
                }
            })
            return res
        } catch (err) {
            return err
        }
}

const checkPaymentStatus = async (tokenID) => {
    const data = {
        method: "get",
        table: "cxs-customer-payments-prd",
        keys: {
            tokenPaymentId: tokenID
        }
    }
    const domain = 'https://cxs.globe.com.ph';
    
    try {
        const res = await axios.post(`${domain}/connectivityTest/dynamo`, data, {
            headers: {
                "Content-Type": "application/json",
                "User-Agent": "PostmanRuntime/7.35.0",
                "Accept": "*/*",
                "Accept-Encoding": "gzip, deflate, br",
                "Connection": "keep-alive",
                "CxsCacheControl": `no-get-cache`
            }
        })
        return res
    } catch (err) {
        console.error(err)
    }
}

module.exports = {
    paymentSessionCallback,
    checkPaymentStatus
}