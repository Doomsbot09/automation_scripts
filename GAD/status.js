const handler = require('./controllers/index');
const excelToJson = require('convert-excel-to-json');

(async() => {
    const mockData = excelToJson({
        sourceFile: './data.xlsx'
    })
    console.log('GetMSISDNDetails is started!')

    mockData.details.forEach(async(msisdn) => {
        const resp = await handler.checkMsisdnStatus(msisdn.A)
        if(resp) {
            console.log('server resp - msisdn', msisdn.A, 'date', resp['data']['result']['Item']['value']['lastApiCall'])
        }
    })
})()