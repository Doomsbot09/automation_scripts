const handler = require('./controllers/index');
const excelToJson = require('convert-excel-to-json');

(async() => {
    const mockData = excelToJson({
        sourceFile: './data.xlsx'
    })

    console.log('server is started!');
    
    try {
        console.log('enrolled accounts is started!');
        const resp = await handler.enrollAccounts(mockData.details)

        if(resp.status === 200) {
            console.log('GAD is started!');
            mockData.details.forEach(async (item) => {
                const resp = await handler.getAccountDetails(item.A)
                if(resp) {
                    console.log('server resp', resp.data)
                }
            })
        }
    } catch (err) {
        console.log('wew', err)
    }
})()

// ENROLL ACCOUNT
// https://stackblitz.com/edit/js-pdxx4j?file=index.js