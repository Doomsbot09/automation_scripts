const axios = require('axios');
const fs = require('fs');

const enrollAccounts = async (mobileNumbers) => {
    if(mobileNumbers.length) {
        let payload = "";
        const accounts = [{
            mobileNumber: '09458837235',
            accountAlias: 'DoomsBot',
            brand: 'prepaid',
            segment: 'mobile',
            channel: ['SuperApp'],
            brandDetail: 'GHP-Prepaid',
            quickAction: [
              'GHP-Prepaid|SecondaryDefault|1',
              'GHP-Prepaid|SecondaryDefault|2',
              'GHP-Prepaid|SecondaryDefault|3',
              'GHP-Prepaid|SecondaryDefault|4',
              'GHP-Prepaid|SecondaryDefault|5',
              'GHP-Prepaid|SecondaryDefault|6',
              'GHP-Prepaid|SecondaryDefault|7',
            ],
            position: 0,
            createdAt: '2023-10-11T23:26:41+08:00',
            updatedAt: '2023-12-04T13:32:55+08:00',
          }];

        mobileNumbers.forEach((item) => {
            const map = {
                brand: 'postpaid',
                segment: 'mobile',
                channel: ['SuperApp'],
                mobileNumber: `0${item.A}`,
                accountAlias: 'GAD',
                createdAt: '2021-10-11T21:54:44+08:00',
                quickAction: [
                  '|SecondaryDefault|1',
                  '|SecondaryDefault|2',
                  '|SecondaryDefault|3',
                  '|SecondaryDefault|4',
                  '|SecondaryDefault|5',
                  '|SecondaryDefault|6',
                  '|SecondaryDefault|7',
                ],
                updatedAt: '2023-09-25T15:53:22+08:00',
              };
              accounts.push(map);
        })

        payload = JSON.stringify(accounts)

        const data = {
            method: "put",
            table: "cxs-enrolled-accounts-prd",
            keys: {
                user_uuid: "8f462e21-4c17-4551-9d15-01986f509127",
                enrollAccounts: payload
            }
        }
        const domain = 'https://cxs.globe.com.ph';
        try {
            const res = await axios.post(`${domain}/connectivityTest/dynamo`, data, {
                headers: {
                    "Content-Type": "application/json",
                    "User-Agent": "PostmanRuntime/7.35.0",
                    "Accept": "*/*",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Connection": "keep-alive",
                }
            })
            return res
        } catch (err) {
            return err
        }
    }
}

const getAccountDetails = async (msisdn) => {
    const auth = await fs.readFileSync('./tokens/auth.pem', 'utf8');
    const userToken = await fs.readFileSync('./tokens/userAuth.pem', 'utf8')

    const domain = 'https://cxs.globe.com.ph';
    
    try {
        const res = await axios.get(`${domain}/v3/accountManagement/accounts?mobileNumber=${msisdn}&brand=postpaid&segment=mobile`, {
            headers: {
                "User-Agent": "PostmanRuntime/7.35.0",
                "Accept": "*/*",
                "Accept-Encoding": "gzip, deflate, br",
                "Connection": "keep-alive",
                "Authorization": `Bearer ${auth}`,
                "User-Token": `Bearer ${userToken}`,
                "CxsCacheControl": `no-get-cache`
            }
        })

        return res
    } catch (err) {
        console.log(err)
    }
}

const checkMsisdnStatus = async (msisdn) => {
    const data = {
        method: "get",
        table: "ncd-cxszr-pd-api-channels",
        keys: {
            api_key: `GetDetailsByMsisdn:${msisdn}`
        }
    }
    const domain = 'https://cxs.globe.com.ph';
    
    try {
        const res = await axios.post(`${domain}/connectivityTest/dynamo`, data, {
            headers: {
                "Content-Type": "application/json",
                "User-Agent": "PostmanRuntime/7.35.0",
                "Accept": "*/*",
                "Accept-Encoding": "gzip, deflate, br",
                "Connection": "keep-alive",
                "CxsCacheControl": `no-get-cache`
            }
        })
        return res
    } catch (err) {
        console.error(err)
    }
}

module.exports = {
    getAccountDetails,
    checkMsisdnStatus,
    enrollAccounts
}